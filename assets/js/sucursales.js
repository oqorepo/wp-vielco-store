  var map;
  var markerData= [
    {lat:  -33.40596, lng: -70.760364, zoom: 15, name: "Santiago"},
    {lat: -23.531299, lng:  -70.39313, zoom: 15, name: "Antofagasta"},
    {lat: -29.971743, lng: -71.265087, zoom: 16, name: "Coquimbo"}, 
    {lat: -35.419596, lng: -71.633429, zoom: 17, name: "Talca"}, 
    {lat: -36.799776, lng: -73.085315, zoom: 16, name: "Concepción"},
    {lat: -41.471322, lng:  -72.99232, zoom: 17, name: "Puerto Montt"},
  ];
  //google map custom marker icon - .png fallback for IE11
  var is_internetExplorer11= navigator.userAgent.toLowerCase().indexOf('trident') > -1;
  //var marker_url = "/img/cd-icon-location.png" : "/img/cd-icon-location.svg";
  //define the basic color of your map, plus a value for saturation and brightness
  var main_color = '#2d313f',
    saturation_value= -20,
    brightness_value= 5;

  //we define here the style of the map
  var style= [ 
    {
      //set saturation for the labels on the map
      elementType: "labels",
      stylers: [
        {saturation: saturation_value}
      ]
    },  
      { //poi stands for point of interest - don't show these lables on the map 
      featureType: "poi",
      elementType: "labels",
      stylers: [
        {visibility: "off"}
      ]
    },
    {
      //don't show highways lables on the map
          featureType: 'road.highway',
          elementType: 'labels',
          stylers: [
              {visibility: "off"}
          ]
      }, 
    {   
      //don't show local road lables on the map
      featureType: "road.local", 
      elementType: "labels.icon", 
      stylers: [
        {visibility: "off"} 
      ] 
    },
    { 
      //don't show arterial road lables on the map
      featureType: "road.arterial", 
      elementType: "labels.icon", 
      stylers: [
        {visibility: "off"}
      ] 
    },
    {
      //don't show road lables on the map
      featureType: "road",
      elementType: "geometry.stroke",
      stylers: [
        {visibility: "off"}
      ]
    }, 
    //style different elements on the map
    { 
      featureType: "transit", 
      elementType: "geometry.fill", 
      stylers: [
        { hue: main_color },
        { visibility: "on" }, 
        { lightness: brightness_value }, 
        { saturation: saturation_value }
      ]
    }, 
    {
      featureType: "poi",
      elementType: "geometry.fill",
      stylers: [
        { hue: main_color },
        { visibility: "on" }, 
        { lightness: brightness_value }, 
        { saturation: saturation_value }
      ]
    },
    {
      featureType: "poi.government",
      elementType: "geometry.fill",
      stylers: [
        { hue: main_color },
        { visibility: "on" }, 
        { lightness: brightness_value }, 
        { saturation: saturation_value }
      ]
    },
    {
      featureType: "poi.sport_complex",
      elementType: "geometry.fill",
      stylers: [
        { hue: main_color },
        { visibility: "on" }, 
        { lightness: brightness_value }, 
        { saturation: saturation_value }
      ]
    },
    {
      featureType: "poi.attraction",
      elementType: "geometry.fill",
      stylers: [
        { hue: main_color },
        { visibility: "on" }, 
        { lightness: brightness_value }, 
        { saturation: saturation_value }
      ]
    },
    {
      featureType: "poi.business",
      elementType: "geometry.fill",
      stylers: [
        { hue: main_color },
        { visibility: "on" }, 
        { lightness: brightness_value }, 
        { saturation: saturation_value }
      ]
    },
    {
      featureType: "transit",
      elementType: "geometry.fill",
      stylers: [
        { hue: main_color },
        { visibility: "on" }, 
        { lightness: brightness_value }, 
        { saturation: saturation_value }
      ]
    },
    {
      featureType: "transit.station",
      elementType: "geometry.fill",
      stylers: [
        { hue: main_color },
        { visibility: "on" }, 
        { lightness: brightness_value }, 
        { saturation: saturation_value }
      ]
    },
    {
      featureType: "landscape",
      stylers: [
        { hue: main_color },
        { visibility: "on" }, 
        { lightness: brightness_value }, 
        { saturation: saturation_value }
      ]
      
    },
    {
      featureType: "road",
      elementType: "geometry.fill",
      stylers: [
        { hue: main_color },
        { visibility: "on" }, 
        { lightness: brightness_value }, 
        { saturation: saturation_value }
      ]
    },
    {
      featureType: "road.highway",
      elementType: "geometry.fill",
      stylers: [
        { hue: main_color },
        { visibility: "on" }, 
        { lightness: brightness_value }, 
        { saturation: saturation_value }
      ]
    }, 
    {
      featureType: "water",
      elementType: "geometry",
      stylers: [
        { hue: main_color },
        { visibility: "on" }, 
        { lightness: brightness_value }, 
        { saturation: saturation_value }
      ]
    }
  ];

  function initialize() {
      map = new google.maps.Map(document.getElementById('map-canvas'), {
        zoom: 15,
        center: {lat: -33.40596, lng: -70.760364}
      });
      markerData.forEach(function(data) {
        var newmarker= new google.maps.Marker({
          map:map,
          position:{lat:data.lat, lng:data.lng},
          title: data.name
        });
        //jQuery("#selectlocation").append('<option value="'+[data.lat, data.lng,data.zoom].join('|')+'">'+data.name+'</option>');
      });
  }

  google.maps.event.addDomListener(window, 'load', initialize);

  jQuery(document).ready(function() {
    jQuery(document).on('click','#s-santiago',function() {
      map.setZoom(15);
      map.setCenter({lat:-33.40596, lng:-70.760364});
      jQuery('.caja_sucursal').removeClass("active");
      jQuery(this).addClass("active");  
    });
    jQuery(document).on('click','#s-antofagasta',function() {
      map.setZoom(15);
      map.setCenter({lat:-23.531299, lng:-70.39313});
      jQuery('.caja_sucursal').removeClass("active");
      jQuery(this).addClass("active");
    });
    jQuery(document).on('click','#s-coquimbo',function() {
      map.setZoom(16);
      map.setCenter({lat:-29.971743, lng:-71.265087});
      jQuery('.caja_sucursal').removeClass("active");
      jQuery(this).addClass("active");
    });
    jQuery(document).on('click','#s-talca',function() {
      map.setZoom(17);
      map.setCenter({lat:-35.419596, lng:-71.633429});
      jQuery('.caja_sucursal').removeClass("active");
      jQuery(this).addClass("active");
    });
    jQuery(document).on('click','#s-concepcion',function() {
      map.setZoom(16);
      map.setCenter({lat:-36.799776, lng:-73.085315});
      jQuery('.caja_sucursal').removeClass("active");
      jQuery(this).addClass("active");
    });
    jQuery(document).on('click','#s-puerto-montt',function() {
      map.setZoom(17);
      map.setCenter({lat:-41.471322, lng:-72.99232});
      jQuery('.caja_sucursal').removeClass("active");
      jQuery(this).addClass("active");
    });
  }); 

/*
  jQuery(document).on('change','#selectlocation',function() {
    var latlngzoom = jQuery(this).val().split('|');
    var newzoom = 1*latlngzoom[2],
    newlat = 1*latlngzoom[0],
    newlng = 1*latlngzoom[1];
    map.setZoom(newzoom);
    map.setCenter({lat:newlat, lng:newlng});
  });
*/
