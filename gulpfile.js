const gulp = require('gulp'),
    browserSync = require('browser-sync').create(), //Recarga el navegador
    autoprefixer = require('gulp-autoprefixer'), //Autoprefixer
    notify = require('gulp-notify'), //Notificaciones
    sass = require('gulp-sass'), //Copila SCSS
    sourcemaps = require('gulp-sourcemaps'), //SourceMaps
    gutil = require( 'gulp-util' ),
    ftp = require( 'vinyl-ftp' );

gulp.task('sass', function () {
    return gulp.src('./assets/scss/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'expanded',
            sourceComments: true
        }))
        .on("error", notify.onError({
            sound: true,
            title: 'Error de copilación SCSS'
        }))
        .pipe(autoprefixer({
            versions: ['last 2 browsers']
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./assets/css/'))
        .pipe(browserSync.stream({
            match: '**/*.css'
        }));
});

gulp.task( 'deploy', function () {
 
    var conn = ftp.create( {
        host:     '205.186.128.218',
        user:     'neourbe',
        password: 'Neourbe2017.',
        parallel: 4,
        log:      gutil.log
    } );
 
    var globs = [
        './assets/**',
        './*.png',
        './*.php',
        './*.css'
    ];
 
    // using base = '.' will transfer everything to /public_html correctly
    // turn off buffering in gulp.src for best performance
 
    return gulp.src( globs, { base: '.', buffer: false } )
        .pipe( conn.newerOrDifferentSize( '/public_html/wp-content/themes/ninezeroseven-child' ) ) // only upload newer files
        .pipe( conn.dest( '/public_html/wp-content/themes/ninezeroseven-child' ) );
 
} );

gulp.task('watch', function () {
    browserSync.init({
        proxy: 'http://localhost/neourbe/',
        files: [
            './*.php',
            './*.css',
            './*.js',
            './*.scss',
            './*.jpg',
            './*.png'
        ],
        notify: true
    });
    gulp.watch('./**/*.scss', ['sass']);
    gulp.watch('./**/*.php').on('change', browserSync.reload);
    gulp.watch('./**/*.js').on('change', browserSync.reload);
});

gulp.task('default', ['sass']);