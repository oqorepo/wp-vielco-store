<?php
/**
 * Electro Child
 *
 * @package electro-child
 */

/**
 * Include all your custom code here
 */

function vs_enqueues() {
    
    if (is_page('sucursales')) {
        
        wp_register_script('map-sucursales', '//maps.googleapis.com/maps/api/js?key=AIzaSyB3lQGrpyeK0y2RHFZYVeEivMkSjsQPSQg' );
        wp_register_script('vs-sucursales', get_stylesheet_directory_uri() . '/assets/js/sucursales.js');
        wp_enqueue_script('map-sucursales');
        wp_enqueue_script('vs-sucursales');

    }

}
add_action('wp_enqueue_scripts', 'vs_enqueues');